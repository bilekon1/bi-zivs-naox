
#pragma once
#define BOOST_SIGNALS_NO_DEPRECATION_WARNING

#include <alcommon/alproxy.h>
#include <alproxies/almotionproxy.h>
#include <alproxies/alrobotpostureproxy.h>
#include <alproxies/alautonomouslifeproxy.h>
#include <alproxies/altexttospeechproxy.h>
#include <alproxies/alconnectionmanagerproxy.h>

using namespace std;

class Robot{
public:
	Robot();
	void poseInit();

	void aLifeOff();
	void say(const string&);
	void connect();
	const string getState();

	void stiffnessOn(const string&);
	void stiffnessOff(const string&);

	void openHand(const string&);
	void closeHand(const string&);

	void moveJoint(int, const string&);
	void moveWrist(int, const string&);
	void moveLegs(int, int);

	void wakeUp();
	void moveInit();
	void rest();

private:
	bool connected;
	
	AL::ALMotionProxy* motion;
	AL::ALRobotPostureProxy* posture;
	AL::ALAutonomousLifeProxy* alife;
	AL::ALTextToSpeechProxy* speech;
	AL::ALConnectionManagerProxy* connection;

};