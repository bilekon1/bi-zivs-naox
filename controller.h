
#pragma once
#define BOOST_SIGNALS_NO_DEPRECATION_WARNING

#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <boost/thread.hpp> 
#include <boost/date_time/posix_time/posix_time.hpp>
#include "joystick.h"
#include "robot.h"

class Controller{
public:
	Controller(Robot*);	

private:
	void initController();

	void tick(void);
	void checkButtons();

	void tickBody();
	void tickLArm();
	void tickRArm();

	void setBody();
	void setLArm();
	void setRArm();

	void togglePause();
	void toggleLHand();
	void toggleRHand();

	int joy_fd;
	int activeBodyPart;

	bool paused;
	bool LHandOpen;
	bool RHandOpen;

	Robot* robot;
	struct joystick* joy;
};