
#include "controller.h"

Controller::Controller(Robot* r) : robot(r){	
	initController();
	activeBodyPart = 1;
	paused = true;
	LHandOpen = false;
	RHandOpen = false;
}

void Controller::initController(){
	joy = (joystick*) malloc(sizeof *joy);
	joy_fd = open_joy();

	if(joy_fd < 0){
		cout << "[NAOX] Cannot open controller " << JOY_NAME << "fd: " << joy_fd << endl;
		robot->say("Remote lost.");
	}else{
		cout << "[NAOX] Controller connected" << endl;
		boost::thread t(&Controller::tick, this);
	}
}

void Controller::tick(){	
	checkButtons();
	//cout << "[NAOX][DEBUG]" << setw(3) << joy->s1_x <<"|"<< joy->s1_y <<"|"<< joy->s2_x <<"|"<< joy->s2_y <<"|"<< joy->btn << '\r' << flush;

	if (!paused){

		if (joy->btn == 11)	toggleLHand();
		if (joy->btn == 12)	toggleRHand();

		switch (activeBodyPart){
		case 1:
			tickBody();
			break;
		case 2:
			tickLArm();
			break;
		case 3:
			tickRArm();
			break;
		default:
			break;
		}
	} else {
		boost::posix_time::seconds secTime(3);
		boost::this_thread::sleep(secTime); 
	}
	tick();
}

void Controller::checkButtons(){
	get_joy(joy_fd, joy);

	if (joy->btn == 5)		setBody();
	if (joy->btn == 8)		setLArm();
	if (joy->btn == 6)		setRArm();
	if (joy->btn == 10)		togglePause();
	if (joy->btn == 3)		robot->connect();
	if (joy->btn == 1)		robot->say("I am" + robot->getState());

}

void Controller::tickBody(){
	robot->moveJoint(joy->s1_x, "HeadYaw");
	robot->moveJoint(joy->s1_y, "HeadPitch");
	robot->moveLegs(joy->s2_y, joy->s2_x);	
}

void Controller::tickLArm(){
	robot->moveJoint(joy->s1_y, "LShoulderPitch");
	robot->moveJoint(joy->s1_x, "LShoulderRoll");
	robot->moveJoint(joy->s2_x, "LElbowYaw");
	robot->moveJoint(joy->s2_y, "LElbowRoll");
	robot->moveWrist(joy->btn, "LWristYaw");	
}

void Controller::tickRArm(){
	robot->moveJoint(joy->s1_y, "RShoulderPitch");
	robot->moveJoint(joy->s1_x, "RShoulderRoll");
	robot->moveJoint(joy->s2_x, "RElbowYaw");
	robot->moveJoint(joy->s2_y, "RElbowRoll");
	robot->moveWrist(joy->btn, "RWristYaw");	
}

void Controller::setBody(){
	activeBodyPart = 1;
	robot->stiffnessOn("HeadYaw");
	robot->stiffnessOn("HeadPitch");
	cout << "[NAOX] Body and movement" << endl;
}

void Controller::setLArm(){
	activeBodyPart = 2;
	robot->stiffnessOn("LShoulderPitch");
	robot->stiffnessOn("LShoulderRoll");
	robot->stiffnessOn("LElbowYaw");
	robot->stiffnessOn("LElbowRoll");
	robot->stiffnessOn("LWristYaw");
	robot->stiffnessOn("LHand");
	cout << "[NAOX] Left arm" << endl;
}

void Controller::setRArm(){
	activeBodyPart = 3;
	robot->stiffnessOn("RShoulderPitch");
	robot->stiffnessOn("RShoulderRoll");
	robot->stiffnessOn("RElbowYaw");
	robot->stiffnessOn("RElbowRoll");
	robot->stiffnessOn("RWristYaw");
	robot->stiffnessOn("RHand");
	cout << "[NAOX] Right arm" << endl;
}

void Controller::togglePause(){
	if (paused){
		paused = false;
		robot->say("Remote on.");
		robot->wakeUp();
		robot->moveInit();

		cout << "[NAOX] Resumed" << endl;
	}
	else{
		paused = true;
		robot->say("Remote off.");
		robot->rest();
		cout << "[NAOX] Paused" << endl;
	}
}

void Controller::toggleLHand(){
	if (LHandOpen){
		robot->closeHand("LHand");
		LHandOpen = false;
	}
	else{
		robot->openHand("LHand");
		LHandOpen = true;
	}
}

void Controller::toggleRHand(){
	if (RHandOpen){
		robot->closeHand("RHand");
		RHandOpen = false;
	}
	else{
		robot->openHand("RHand");
		RHandOpen = true;
	}
}
