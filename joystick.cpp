
#include "joystick.h"

int open_joy()
{
    int joy_fd = open(JOY_NAME, O_RDONLY);    
    return joy_fd;
}

int get_joy(int fd, joystick* j)
{
    char buf[7];
    int r = read(fd, buf, sizeof(buf));

    j->s2_x = (buf[0] & 0xff)-128;
    j->s2_y = ((buf[1] & 0xff)-127)*(-1);
    j->s1_x = (buf[2] & 0xff)-128;
    j->s1_y = ((buf[3] & 0xff)-127)*(-1);
    j->b1 = buf[4] & 0xff;
    j->b2 = buf[5] & 0xff;
    j->btn = get_btn(j);

    return 0;
}

int get_btn(joystick* j)
{
    switch(j->b1){
        case 31: return 1;
        case 47: return 2;
        case 79: return 3;
        case 143: return 4;
        case 0: return 5;
        case 2: return 6;
        case 4: return 7;
        case 6: return 8;
    }

    switch(j->b2){
        case 16: return 9;
        case 32: return 10;
        case 1: return 11;
        case 2: return 12;
        case 4: return 13;
        case 8: return 14;
        case 64: return 15;
        case 128: return 16;
    }

    return 0;
}