
#include "module.h"

#include <iostream>
#include <alcommon/albroker.h>

NaoxModule::NaoxModule(boost::shared_ptr<AL::ALBroker> broker, const std::string& name) : AL::ALModule(broker, name)
{
  setModuleDescription("NAOX module - Ondřej Bílek - FIT ČVUT");
}

NaoxModule::~NaoxModule()
{

}

void NaoxModule::init()
{
  Naox* nao = new Naox();
}
