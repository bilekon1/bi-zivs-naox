
#pragma once
#define BOOST_SIGNALS_NO_DEPRECATION_WARNING

#include <alcommon/almodule.h>
#include "naox.h"

namespace AL
{
  class ALBroker;
}

class NaoxModule : public AL::ALModule
{
public:
  NaoxModule(boost::shared_ptr<AL::ALBroker> broker,const std::string &name);

  virtual ~NaoxModule();
  virtual void init();
};
