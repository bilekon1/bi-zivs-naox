
#pragma once
#define BOOST_SIGNALS_NO_DEPRECATION_WARNING

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define JOY_NAME "/dev/hidraw0"

struct joystick {
          int s1_x;
          int s1_y;
          int s2_x;
          int s2_y;
          int b1;
          int b2;
          int btn;
 };

extern int open_joy();
extern int get_joy(int fd, joystick* j);
extern int get_btn(joystick* j);

