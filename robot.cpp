
#include "robot.h"

Robot::Robot(){
	try{
		motion = new AL::ALMotionProxy("127.0.0.1" , 9559);
		posture = new AL::ALRobotPostureProxy("127.0.0.1" , 9559);
		alife = new AL::ALAutonomousLifeProxy("127.0.0.1" , 9559);
		speech = new AL::ALTextToSpeechProxy("127.0.0.1" , 9559);
		connection = new AL::ALConnectionManagerProxy("127.0.0.1" , 9559);
	} catch (const AL::ALError& e){
		cout << "[NAOX] Robot connection failed" << endl;
	}
	connected = true;
	cout << "[NAOX] Robot connected" << endl;
}

void Robot::poseInit(){
	posture->goToPosture("StandInit", 1.0f);
}

void Robot::say(const string& txt){
	speech->setLanguage("English");
	speech->say(txt);
}

void Robot::connect(){
	connection->connect("10.10.48.252");
}

const string Robot::getState(){
	return connection->state();
}

void Robot::aLifeOff(){
	alife->setState("disabled");
}

void Robot::stiffnessOn(const string& joint){
	motion->stiffnessInterpolation(joint, 1.0f, 0.5f);
}

void Robot::stiffnessOff(const string& joint){
	motion->stiffnessInterpolation(joint, 0.0f, 0.5f);
}

void Robot::openHand(const string& hand){
	motion->openHand(hand);
}

void Robot::closeHand(const string& hand){
	motion->closeHand(hand);
}

void Robot::moveJoint(int x, const string& joint){
	if (x > 120)					motion->changeAngles(joint, -0.1f, 0.2f);
	if (x < -120)					motion->changeAngles(joint, 0.1f, 0.2f);
	if (x >= 75 && x <= 120)		motion->changeAngles(joint, -0.1f, 0.1f);
	if (x <= -75 && x >= -120)		motion->changeAngles(joint, 0.1f, 0.1f);
	if (x > 25 && x < 75)			motion->changeAngles(joint, -0.1f, 0.05f);
	if (x < -25 && x < -75)		motion->changeAngles(joint, 0.1f, 0.05f);
}

void Robot::moveWrist(int x, const string& joint){
	if (x == 13)		motion->changeAngles(joint, -0.1f, 0.4f);
	if (x == 14)		motion->changeAngles(joint, 0.1f, 0.4f);
}

void Robot::moveLegs(int RY, int RX){
	if (motion->moveIsActive()){
		if (RY < 50 && RY > -50 && RX < 50 && RX > -50)		motion->stopMove();
	}
	else{
		if (RY > 50)	motion->move(1.0f, 0.0f, 0.0f);
		if (RY < -50)	motion->move(-1.0f, 0.0f, 0.0f);
		if (RX > 50)	motion->move(0.0f, 0.0f, -1.0f);
		if (RX < -50)	motion->move(0.0f, 0.0f, 1.0f);
	}
}

void Robot::wakeUp(){
	motion->wakeUp();
}

void Robot::moveInit(){
	motion->moveInit();
}

void Robot::rest(){
	motion->rest();
}
